import React from "react";
import styled from "styled-components";

const Container = styled.div`
    padding: 6rem 0 0;
    min-height: 100vh;
`;

const Contact = () => {
    return <Container>Contact</Container>;
};

export default Contact;
