import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";
import styled from "styled-components";

const Container = styled.div`
    max-height: inherit;
`;

const Spinner = ({ shown }) => {
    return (
        <Container>
            <Dimmer active={shown} inverted>
                <Loader />
            </Dimmer>
        </Container>
    );
};

export default Spinner;
