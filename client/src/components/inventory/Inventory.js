import React, { useLayoutEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getInventory } from "../../redux/actions/inventory";
import { v4 } from "uuid";
import Spinner from "../universal/Spinner";
import styled from "styled-components";

import { Grid } from "semantic-ui-react";

const Container = styled.div`
    padding: 7rem 0 0;
    min-height: 100vh;
`;

const ImageContainer = styled.div`
    max-height: 200px;
    width: 300px;
    box-sizing: border-box;
    overflow: hidden;
    background-color: black;
`;

const Image = styled.img`
    width: 300px;
    min-height: 200px;
    margin: auto;
    @media (max-width: 680px) {
        width: fit-content;
        border-right: none;
    }
`;

const InfoContainer = styled.div`
    padding: 0.3rem;
`;

const TitleText = styled.div`
    font-size: 1rem;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
`;

const BodyText = styled.div`
    overflow: hidden;
    text-overflow: ellipsis;
`;

const InventoryItem = ({ name, description }) => {
    return (
        <Grid.Column style={{ width: "max-content", margin: ".3rem 0" }}>
            <div>
                <ImageContainer>
                    <Image />
                </ImageContainer>
                <InfoContainer>
                    <TitleText>{name}</TitleText>
                    <BodyText>{description}</BodyText>
                </InfoContainer>
            </div>
        </Grid.Column>
    );
};

InventoryItem.propTypes = {
    name: PropTypes.string,
    description: PropTypes.string
};

const InventoryContainer = ({ car_items, loading }) => {
    if (!loading && car_items.length === 0) return <div>No Items</div>;
    return (
        <Grid divided className="centered">
            {car_items.map((item) => {
                const { name, description } = item;
                return <InventoryItem key={v4()} name={name} description={description} />;
            })}
        </Grid>
    );
};

const Inventory = ({ getInventory, loading, car_items, fetch_car_items }) => {
    useLayoutEffect(() => {
        if (!loading && fetch_car_items) {
            getInventory();
        }
    }, []);

    return (
        <Container>
            {loading && <Spinner />}
            <InventoryContainer car_items={car_items} loading={loading} />
        </Container>
    );
};

Inventory.propTypes = {
    getInventory: PropTypes.func.isRequired,
    loading: PropTypes.bool,
    car_items: PropTypes.array,
    fetch_car_items: PropTypes.bool
};

const mapStateToProps = (state) => ({
    car_items: state.inventory.car_items,
    loading: state.inventory.loading,
    fetch_car_items: state.inventory.fetch_car_items
});

const mapDispatchToProps = {
    getInventory
};

export default connect(mapStateToProps, mapDispatchToProps)(Inventory);
